<?php

/**
 * Template Name: Single Ticket
 *
 */
get_header(); ?>

<div id="primary" class="content-area container">
	<main id="main" class="site-main" role="main" style="padding: 40px 0;">
			<?php
			// Start the loop.
			while (have_posts()) : the_post(); ?>
				<div class="row">
					<div class="col-md-6">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<header class="entry-header">
								<?php the_title('<h1 class="entry-title">', '</h1>'); ?>
							</header><!-- .entry-header -->
							<div class="entry-content">
								<?php
								the_content();

								wp_link_pages(
									array(
										'before'      => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'twentysixteen') . '</span>',
										'after'       => '</div>',
										'link_before' => '<span>',
										'link_after'  => '</span>',
										'pagelink'    => '<span class="screen-reader-text">' . __('Page', 'twentysixteen') . ' </span>%',
										'separator'   => '<span class="screen-reader-text">, </span>',
									)
								);

								if ('' !== get_the_author_meta('description')) {
									get_template_part('template-parts/biography');
								}

								?>
								<div class="custom-meta">
									<p>
										<?php
										echo get_avatar(get_custom_field('your-email')) . '<br>';
										echo get_the_date() . ' ' . get_the_time() . '<br>';
										echo 'Name: ' . get_custom_field('your-name') . '<br>';
										echo 'Email: ' . get_custom_field('your-email') . '<br>';
										?>
									</p>
								</div>
								
							</div><!-- .entry-content -->
							<?php
						// If comments are open or we have at least one comment, load up the comment template.
						if (comments_open() || get_comments_number()) {
							comments_template();
						} ?>

							
						</article><!-- #post-## -->
					</div>
					<div class="col-md-6">
						<div class="tiket">
								<?php 
								$metadata = get_metadata('post',$post->ID);
								$upload_dir    = wp_upload_dir();
								$cfdb7_dirname = $upload_dir['baseurl'].'/test/';

								if($metadata):
								?>
								<div>
									<ul>
									<?php 
									foreach($metadata as $key=>$meta): 
									?>
										<li><strong><?= $key . ':' ?> </strong>
											<?php foreach($meta as $kez=>$value){
												if(its_file($value)){
													$value = '<a href="'. $cfdb7_dirname . $value  .'" target="_blank">'. $value .'</a>';
												}
												if($key>0){echo ', '.  $value;} 
												else{ echo $value;}
											} ?>
										</li>

										<?php endforeach; ?>
									</ul>

								</div>

								<?php endif; ?>

							</div>

						
					</div>
				</div>
			<?php
			// End of the loop.
			endwhile;
			?>
	</main><!-- .site-main -->

	<?php get_sidebar('content-bottom'); ?>

</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>