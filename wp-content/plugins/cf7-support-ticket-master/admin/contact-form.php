<?php 
$metadata = get_metadata('post',$object->ID);
$upload_dir    = wp_upload_dir();
$cfdb7_dirname = $upload_dir['baseurl'].'/test/';

if($metadata):
?>
<div>
    <ul>
    <?php 
     foreach($metadata as $key=>$meta): 
     ?>
        <li><strong><?= $key . ':' ?> </strong>
            <?php foreach($meta as $kez=>$value){
                if(its_file($value)){
                    $value = '<a href="'. $cfdb7_dirname . $value  .'" target="_blank">'. $value .'</a>';
                }
                if($key>0){echo ', '.  $value;} 
                else{ echo $value;}
             } ?>
        </li>

        <?php endforeach; ?>
    </ul>

</div>

<?php endif; 
