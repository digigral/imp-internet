<?php

/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

$container = get_theme_mod('understrap_container_type');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="/wp-content/themes/imp-internet/img/favicon.ico" type="image/x-icon">
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-79T7GK1YN3"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-79T7GK1YN3');
	</script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php do_action('wp_body_open'); ?>
	<?php
    get_template_part('/page-templates/page-parts/modal-img');
    ?>
	<div class="site" id="page">

		<!-- ******************* The Navbar Area ******************* -->
		<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

			<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e('Skip to content', 'understrap'); ?></a>

			<nav class="navbar navbar-expand-md">

				<?php if ('container' == $container) : ?>
					<div class="container">
						<?php endif; ?>

						<a class="logo" href="/">
							<img src="/wp-content//themes/imp-internet/img/imp-logo-header.png">
						</a>
						<div class="right">
							<div class="home-it">
								<a href="/">
									<img src="/wp-content/themes/imp-internet/img/domov.png">
									<div>Home</div>
								</a>
							</div>
							<div class="account">
								<a href="/account">
									<svg>
										<use xlink:href="#svg_user" />
									</svg>
									<div>Account</div>
								</a>
							</div>
						</div>
					<?php if ('container' == $container) : ?>
					</div><!-- .container -->
				<?php endif; ?>

			</nav><!-- .site-navigation -->

		</div><!-- #wrapper-navbar end -->
		<div class="container">
			<div class="language-swicher" style="text-align: right;">
				<?php echo do_shortcode('[gtranslate]'); ?>
			</div>
		</div>
