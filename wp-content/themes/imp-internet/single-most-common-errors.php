<?php

/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();
$container = get_theme_mod('understrap_container_type');
$errors = get_field('napaka_na_pumpi');
$pumps = [];
if ($errors) {
	foreach ($errors as $error) {
		$pumps[] = $error['pumpa'];
	}
}
?>

<div class="wrapper single-most-common-errors" id="single-wrapper">

	<div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">
		<div class="row">
			<div class="col-md-10">
				<h1 class="page-title"><?= get_the_title(); ?></h1>
				<div class="row">
					<div class="col-md-6">
						<div>
							<?= get_field('splosni_opis'); ?>
						</div>
						<?php if ($pumps) : ?>
							<h3>PUMPS: </h3>
							<div class="pumps__list">
								<?php foreach ($pumps as $pump) : ?>
									<a href="#<?= slugify($pump); ?>"><?= $pump; ?></a>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
					<div class="col-md-6">
						<?php if(get_field('slika')): ?>
						<div class="main-photo">
							<img src="<?= get_field('slika'); ?>" class="main-photo_img">
						</div>
						<?php endif; ?>
					</div>
				</div>
				<?php
				if ($errors) : ?>
					<div class="errors__list">
						<?php
						foreach ($errors as $error) : ?>
							<div class="errors__item" id="<?= slugify($error['pumpa']); ?>">
								<h3><?= $error['pumpa']; ?></h3>
								<div><?= $error['opis_napake_na_pumpi']; ?></div>
								<?php if ($error['galerija_slik']) : ?>
									<div class="galery__list">
										<?php
										foreach ($error['galerija_slik'] as $key=>$slika) :
										?>
											<div class="galery__item">
												<img src="<?= $slika ?>" class="galery__item__<?= $key; ?>">
											</div>
										<?php endforeach; ?>
									</div>
								<?php endif; ?>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2">
				<a class="back__btn" href="<?= get_post_type_archive_link('most-common-errors'); ?>">
					< Back </a>
			</div>
		</div>
	</div><!-- #content -->

</div><!-- #single-wrapper -->

<?php get_footer(); ?>