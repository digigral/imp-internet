<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="single-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
		<div class="row">
			<div class="col-md-10">
				<h1 class="page-title"><?= get_the_title(); ?></h1>
				<video id="noContextMenu" controls controlsList="nodownload" class="home-visual__video" width="100%">
					<source src="<?= get_field('video') ?>"
							type="video/mp4">
					Your browser does not support the video tag.
				</video>
				<div>
					<?= get_field('opis'); ?>
				</div>
			</div>
			<div class="col-md-2">
				<a class="back__btn" href="<?= get_post_type_archive_link('videos'); ?>">
					<- back </a>
			</div>
	</div><!-- #content -->

</div><!-- #single-wrapper -->

<?php get_footer(); ?>
