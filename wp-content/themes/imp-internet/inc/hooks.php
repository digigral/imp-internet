<?php
/**
 * Custom hooks.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'understrap_site_info' ) ) {
	/**
	 * Add site info hook to WP hook library.
	 */
	function understrap_site_info() {
		do_action( 'understrap_site_info' );
	}
}

if ( ! function_exists( 'understrap_add_site_info' ) ) {
	add_action( 'understrap_site_info', 'understrap_add_site_info' );

	/**
	 * Add site info content.
	 */
	function understrap_add_site_info() {
		$the_theme = wp_get_theme();

		$site_info = sprintf(
			'<a href="%1$s">%2$s</a><span class="sep"> | </span>%3$s(%4$s)',
			esc_url( __( 'http://wordpress.org/', 'understrap' ) ),
			sprintf(
				/* translators:*/
				esc_html__( 'Proudly powered by %s', 'understrap' ),
				'WordPress'
			),
			sprintf( // WPCS: XSS ok.
				/* translators:*/
				esc_html__( 'Theme: %1$s by %2$s.', 'understrap' ),
				$the_theme->get( 'Name' ),
				'<a href="' . esc_url( __( 'http://understrap.com', 'understrap' ) ) . '">understrap.com</a>'
			),
			sprintf( // WPCS: XSS ok.
				/* translators:*/
				esc_html__( 'Version: %1$s', 'understrap' ),
				$the_theme->get( 'Version' )
			)
		);

		echo apply_filters( 'understrap_site_info_content', $site_info ); // WPCS: XSS ok.
	}
}

populate_roles_my();
function populate_roles_my() {
	add_role( 'serviserji', 'Serviserji' );
	$role = get_role( 'serviserji' );
	$role->add_cap( 'read' );
	$role->add_cap( 'level_0' );

	add_role( 'prodajlci_distributerji', 'Prodajlci, distributerji' );
	$role = get_role( 'prodajlci_distributerji' );
	$role->add_cap( 'read' );
	$role->add_cap( 'level_0' );

	add_role( 'instalaterji', 'Inštalaterji' );
	$role = get_role( 'instalaterji' );
	$role->add_cap( 'read' );
	$role->add_cap( 'level_0' );
}


add_action( 'pre_get_posts', 'sk_change_portfolio_posts_order' );
/**
 * Order entries by title on Portfolio CPT archive.
 *
 * @link http://www.billerickson.net/customize-the-wordpress-query/
 * @param object $query data
 *
 */
function sk_change_portfolio_posts_order( $query ) {
    if ( $query->is_main_query() && !is_admin() && is_post_type_archive( 'error-codes' ) ) {
        $query->set( 'order', 'ASC' );
        $query->set( 'orderby', 'title' );
    }
}