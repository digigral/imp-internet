<?php

/**
 *
 *  Custom Post Type registration
 *
 */

function register_my_posttypes()
{

  /*
   *
   * example cpt
   * replace example with your own cpt labels
	 * replace digi with your own text domain
   */

  $labels = array(
      'name'               => _x( 'Exploded view', 'post type general name', 'digi' ),
      'singular_name'      => _x( 'Eksplozijske risba', 'post type singular name', 'digi' ),
      'menu_name'          => _x( 'Exploded view', 'admin menu', 'digi' ),
      'name_admin_bar'     => _x( 'Exploded view', 'add new on admin bar', 'mdigi' ),
      'add_new'            => _x( 'Add new Eksplozijske risba', 'slider', 'digi' ),
      'add_new_item'       => __( 'Add new item', 'digi' ),
      'new_item'           => __( 'New Eksplozijske risba', 'digi' ),
      'edit_item'          => __( 'Edit Eksplozijske risba', 'digi' ),
      'view_item'          => __( 'View Eksplozijske risba', 'digi' ),
      'all_items'          => __( 'All Exploded view', 'digi' ),
      'search_items'       => __( 'Search Exploded view', 'digi' ),
      'parent_item_colon'  => __( 'Parent Exploded view:', 'digi' ),
      'not_found'          => __( 'Not found', 'digi' ),
      'not_found_in_trash' => __( 'Not found in trash', 'digi' )
  );

  $args = array(
      'labels'             => $labels,
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'exploded-view', 'with_front' => true ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'taxonomies' => array(''),
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => array( 'title', 'author', 'thumbnail', 'excerpt', 'revisions' )
  );

  register_post_type( 'exploded-view', $args );

  register_taxonomy(
        'kategorija-risb',
        'exploded-view',
        array(
          'rewrite' => array(
           "slug" => 'kategorija'
          ),
          'label' => __("Kategorije risb"),
          'hierarchical' => true
        )
      );
  register_taxonomy_for_object_type( 'exploded-view', 'kategorija-risb' );


	$labels = array(
			'name'               => _x( 'Najpogostejše napake', 'post type general name', 'digi' ),
			'singular_name'      => _x( 'Najpogostejša napaka', 'post type singular name', 'digi' ),
			'menu_name'          => _x( 'Najpogostejše napake', 'admin menu', 'digi' ),
			'name_admin_bar'     => _x( 'Najpogostejše napake', 'add new on admin bar', 'mdigi' ),
			'add_new'            => _x( 'Add new', 'podjetje', 'digi' ),
			'add_new_item'       => __( 'Add new item', 'digi' ),
			'new_item'           => __( 'New Najpogostejša napaka', 'digi' ),
			'edit_item'          => __( 'Edit Najpogostejša napaka', 'digi' ),
			'view_item'          => __( 'View Najpogostejša napaka', 'digi' ),
			'all_items'          => __( 'All Najpogostejše napake', 'digi' ),
			'search_items'       => __( 'Search Najpogostejše napake', 'digi' ),
			'parent_item_colon'  => __( 'Parent Najpogostejše napake:', 'digi' ),
			'not_found'          => __( 'Not found', 'digi' ),
			'not_found_in_trash' => __( 'Not found in trash', 'digi' )
	);

	$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'most-common-errors', 'with_front' => true ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'taxonomies' => array(''),
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'author', 'thumbnail', 'excerpt', 'revisions' )
	);
	register_post_type( 'most-common-errors', $args );

  register_taxonomy(
    'kategorija',
    'most-common-errors',
    array(
      'rewrite' => array(
       "slug" => 'kategorija'
      ),
      'label' => __("Kategorije"),
      'hierarchical' => true
    )
  );
  register_taxonomy_for_object_type( 'most-common-errors', 'kategorija' );
  
  $labels = array(
      'name'               => _x( 'Videos', 'post type general name', 'digi' ),
      'singular_name'      => _x( 'Montažni video', 'post type singular name', 'digi' ),
      'menu_name'          => _x( 'Videos', 'admin menu', 'digi' ),
      'name_admin_bar'     => _x( 'Videos', 'add new on admin bar', 'mdigi' ),
      'add_new'            => _x( 'Add new', 'Montažni video', 'digi' ),
      'add_new_item'       => __( 'Add new item', 'digi' ),
      'new_item'           => __( 'New Montažni video', 'digi' ),
      'edit_item'          => __( 'Edit Montažni video', 'digi' ),
      'view_item'          => __( 'View Videos', 'digi' ),
      'all_items'          => __( 'All Videos', 'digi' ),
      'search_items'       => __( 'Search Videos', 'digi' ),
      'parent_item_colon'  => __( 'Parent Montažni video:', 'digi' ),
      'not_found'          => __( 'Not found', 'digi' ),
      'not_found_in_trash' => __( 'Not found in trash', 'digi' )
  );

  $args = array(
      'labels'             => $labels,
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'videos', 'with_front' => true ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'taxonomies' => array(''),
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => array( 'title', 'author', 'thumbnail', 'excerpt', 'revisions' )
  );
  register_post_type( 'videos', $args );
  
   $labels = array(
      'name'               => _x( 'documents', 'post type general name', 'digi' ),
      'singular_name'      => _x( 'Dokument', 'post type singular name', 'digi' ),
      'menu_name'          => _x( 'documents', 'admin menu', 'digi' ),
      'name_admin_bar'     => _x( 'documents', 'add new on admin bar', 'mdigi' ),
      'add_new'            => _x( 'Add new', 'Dokument', 'digi' ),
      'add_new_item'       => __( 'Add new Dokument', 'digi' ),
      'new_item'           => __( 'New Dokument', 'digi' ),
      'edit_item'          => __( 'Edit Dokument', 'digi' ),
      'view_item'          => __( 'View Dokument', 'digi' ),
      'all_items'          => __( 'All documents', 'digi' ),
      'search_items'       => __( 'Search documents', 'digi' ),
      'parent_item_colon'  => __( 'Parent Dokument:', 'digi' ),
      'not_found'          => __( 'Not found', 'digi' ),
      'not_found_in_trash' => __( 'Not found in trash', 'digi' )
  );

  $args = array(
      'labels'             => $labels,
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'documents', 'with_front' => true ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'taxonomies' => array(''),
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
  );
  register_post_type( 'documents', $args );
  
   $labels = array(
      'name'               => _x( 'Error codes', 'post type general name', 'digi' ),
      'singular_name'      => _x( 'Error code', 'post type singular name', 'digi' ),
      'menu_name'          => _x( 'Error codes', 'admin menu', 'digi' ),
      'name_admin_bar'     => _x( 'Error codes', 'add new on admin bar', 'mdigi' ),
      'add_new'            => _x( 'Add new', 'Error code', 'digi' ),
      'add_new_item'       => __( 'Add new Error code', 'digi' ),
      'new_item'           => __( 'New Error code', 'digi' ),
      'edit_item'          => __( 'Edit Error code', 'digi' ),
      'view_item'          => __( 'View Error code', 'digi' ),
      'all_items'          => __( 'All Error codes', 'digi' ),
      'search_items'       => __( 'Search Error codes', 'digi' ),
      'parent_item_colon'  => __( 'Parent Error codes:', 'digi' ),
      'not_found'          => __( 'Not found', 'digi' ),
      'not_found_in_trash' => __( 'Not found in trash', 'digi' )
  );

  $args = array(
      'labels'             => $labels,
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'error-codes', 'with_front' => true ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'taxonomies' => array(''),
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => array( 'title', 'author', 'thumbnail', 'excerpt', 'revisions' )
  );
  register_post_type( 'error-codes', $args );

  $labels = array(
    'name'               => _x( 'FAQ', 'post type general name', 'digi' ),
    'singular_name'      => _x( 'FAQ', 'post type singular name', 'digi' ),
    'menu_name'          => _x( 'FAQ', 'admin menu', 'digi' ),
    'name_admin_bar'     => _x( 'FAQ', 'add new on admin bar', 'mdigi' ),
    'add_new'            => _x( 'Add new', 'FAQ', 'digi' ),
    'add_new_item'       => __( 'Add new FAQ', 'digi' ),
    'new_item'           => __( 'New FAQ', 'digi' ),
    'edit_item'          => __( 'Edit FAQ', 'digi' ),
    'view_item'          => __( 'View FAQ', 'digi' ),
    'all_items'          => __( 'All FAQ', 'digi' ),
    'search_items'       => __( 'Search FAQ', 'digi' ),
    'parent_item_colon'  => __( 'Parent FAQ:', 'digi' ),
    'not_found'          => __( 'Not found', 'digi' ),
    'not_found_in_trash' => __( 'Not found in trash', 'digi' )
);

$args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'faq', 'with_front' => true ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'taxonomies' => array(''),
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title','editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
);
register_post_type( 'faq', $args );

$labels = array(
  'name'               => _x( 'Novice', 'post type general name', 'digi' ),
  'singular_name'      => _x( 'Novica', 'post type singular name', 'digi' ),
  'menu_name'          => _x( 'Novice', 'admin menu', 'digi' ),
  'name_admin_bar'     => _x( 'novice', 'add new on admin bar', 'mdigi' ),
  'add_new'            => _x( 'Add new', 'novica', 'digi' ),
  'add_new_item'       => __( 'Add new novica', 'digi' ),
  'new_item'           => __( 'New novica', 'digi' ),
  'edit_item'          => __( 'Edit novica', 'digi' ),
  'view_item'          => __( 'View novica', 'digi' ),
  'all_items'          => __( 'All novice', 'digi' ),
  'search_items'       => __( 'Search novice', 'digi' ),
  'parent_item_colon'  => __( 'Parent novica:', 'digi' ),
  'not_found'          => __( 'Not found', 'digi' ),
  'not_found_in_trash' => __( 'Not found in trash', 'digi' )
);

$args = array(
  'labels'             => $labels,
  'public'             => true,
  'publicly_queryable' => true,
  'show_ui'            => true,
  'show_in_menu'       => true,
  'query_var'          => true,
  'rewrite'            => array( 'slug' => 'news', 'with_front' => true ),
  'capability_type'    => 'post',
  'has_archive'        => true,
  'taxonomies' => array(''),
  'hierarchical'       => false,
  'menu_position'      => null,
  'supports'           => array( 'title','editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
);
register_post_type( 'news', $args );
}

add_action( 'init', 'register_my_posttypes' );


//Remove from the Side Menu
add_action( 'admin_menu', 'remove_default_post_type' );
 
function remove_default_post_type() {
    remove_menu_page( 'edit.php' );
}
