<?php 

function wpd_testimonials_query( $query ){
    if( ! is_admin()){
        $query->set( 'posts_per_page', 50 );
    }
}
add_action( 'pre_get_posts', 'wpd_testimonials_query' );

function imp_logout_callback ($atts) {
	
    $string ='<div class="my-logout"><a class="btn" href="/logout">Logout</a></div>';
	
	return $string;
}
add_shortcode('imp_logout', 'imp_logout_callback');
