
<!-- The Modal/Lightbox -->
<div id="myModal" class="modal modal__img">
  <div class="modal__img__backdrop"></div>
  <div class="close cursor">
    <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 13 13">
      <path fill="currentColor" d="M11.594.241c.322-.321.843-.321 1.165 0 .321.322.321.843 0 1.165L7.664 6.5l5.095 5.094c.321.322.321.843 0 1.165-.322.321-.843.321-1.165 0L6.5 7.664l-5.094 5.095c-.295.295-.758.32-1.08.074l-.085-.074c-.321-.322-.321-.843 0-1.165L5.336 6.5.241 1.406C-.08 1.084-.08.563.241.24c.322-.321.843-.321 1.165 0L6.5 5.336z" transform="translate(-342 -49) translate(342 49)"></path>
    </svg>
  </div>
  <div class="modal-content">
    <div class="slides">
        <div class="mySlides" data-cap=""> 
            <div class="numbertext">1 / 4</div>
            <img src="" style="width:100%">
        </div>
    </div>


    <!-- Next/previous controls -->
    <a class="prev">&#10094;</a>
    <a class="next">&#10095;</a>

    <!-- Caption text -->
    <div class="caption-container">
      <p id="caption"></p>
    </div>

  </div>
</div>