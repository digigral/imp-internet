<?php
/**
 * Template Name: Preaptive check Template
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

?>
<div class="preaptive-check">
	<div class="container">
		<div class="page-title">
			Preemptive check
		</div>

		<div class="preaptive-check__item preaptive-check__item-first" data-step="0"  id="item-1">
		<div class="preaptive-check_qa">
				<div class="preaptive-check__question">
					Physical damage?
				</div>
				<div class="preaptive-check__a"></div>
				<button class="btn preaptive-check__edit" data-id="item-1">Edit</button>
			</div>
			<div class="preaptive-check__answer">
				<button class="btn answer-yes" data-id="item-2">Yes</button>
				<button class="btn answer-no" data-id="item-4" >No</button>
			</div>
		</div>

		<div class="preaptive-check__item" id="item-2">
			<div class="preaptive-check_qa">
				<div class="preaptive-check__question">
					Damage is the reason of faulty behavoiur?
				</div>
				<div class="preaptive-check__a"></div>
				<button class="btn preaptive-check__edit" data-id="item-2">Edit</button>
			</div>		
			<div class="preaptive-check__answer">
				<button class="btn answer-yes" data-id="item-3">Yes</button>
				<button class="btn answer-no" data-id="item-10" >No</button>
			</div>
		</div>

		

		<div class="preaptive-check__item" id="item-4">
			<div class="preaptive-check_qa">
				<div class="preaptive-check__question">
					Blocked condensation paths?
				</div>
				<div class="preaptive-check__a"></div>
				<button class="btn preaptive-check__edit" data-id="item-4">Edit</button>
			</div>		
			<div class="preaptive-check__answer">
				<button class="btn answer-yes" data-id="item-3">Yes</button>
				<button class="btn answer-no" data-id="item-5" >No</button>
			</div>
		</div>	

		<div class="preaptive-check__item" id="item-5">
			<div class="preaptive-check_qa">
				<div class="preaptive-check__question">
					Obvius marks of dirt or external water on the pump?
				</div>
				<div class="preaptive-check__a"></div>
				<button class="btn preaptive-check__edit" data-id="item-5">Edit</button>
			</div>		
			<div class="preaptive-check__answer">
				<button class="btn answer-yes" data-id="item-3">Yes</button>
				<button class="btn answer-no" data-id="item-6" >No</button>
			</div>
		</div>	

		<div class="preaptive-check__item" id="item-6">
			<div class="preaptive-check_qa">
				<div class="preaptive-check__question">
					Obvius abuse of the pump with inappropriate medium?
				</div>
				<div class="preaptive-check__a"></div>
				<button class="btn preaptive-check__edit" data-id="item-6">Edit</button>
			</div>		
			<div class="preaptive-check__answer">
				<button class="btn answer-yes" data-id="item-3">Yes</button>
				<button class="btn answer-no" data-id="item-7" >No</button>
			</div>
		</div>	

		<div class="preaptive-check__item" id="item-7">
			<div class="preaptive-check_qa">
				<div class="preaptive-check__question">
					Obvious abuse with overvoltage?
				</div>
				<div class="preaptive-check__a"></div>
				<button class="btn preaptive-check__edit" data-id="item-7">Edit</button>
			</div>		
			<div class="preaptive-check__answer">
				<button class="btn answer-yes" data-id="item-3">Yes</button>
				<button class="btn answer-no" data-id="item-20" >No</button>
			</div>
		</div>	

		<div class="preaptive-check__item" id="item-3">
			<div class="preaptive-check_qa">
				<div class="preaptive-check__question">
					Was the pump mounted according to the manual?
				</div>
				<div class="preaptive-check__a"></div>
				<button class="btn preaptive-check__edit" data-id="item-3">Edit</button>
			</div>		
			<div class="preaptive-check__answer">
				<button class="btn answer-yes" data-id="item-20">Yes</button>
				<button class="btn answer-no" data-id="item-10" >No</button>
			</div>
		</div>	

		<div class="preaptive-check__item" id="item-10">
			<div class="preaptive-check_qa">
				<div class="preaptive-check__question">
					Unapproved claim
				</div>
				<div class="preaptive-check__a"></div>
			</div>	
		</div>	
		<div class="preaptive-check__item" id="item-20">
			<div class="preaptive-check_qa">
				<div class="preaptive-check__question">
					Further analysis
				</div>
				<div class="preaptive-check__answer-last">
					<button class="btn send-complaints">Make complaint</button>		
				</div>
			</div>			
		</div>
	</div>
	<div class="preaptive-check__contact">
		<div class="container">
		<?= do_shortcode('[contact-form-7 id="85" title="Contact form 1"]'); ?>
		</div>
	</div>
</div>

<?php
get_footer();
