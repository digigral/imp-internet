<?php
/**
 * Template Name: Print Template
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

// Exit if accessed directly.
if (isset($_GET['id'])) {
    $ID = $_GET['id'];
    $metadata = get_metadata('post', $ID);
    $upload_dir    = wp_upload_dir();
    $cfdb7_dirname = $upload_dir['baseurl'] . '/test/';
    $to_append = '<div class="head"><img src="/wp-content/themes/imp-internet/img/imp-logo-header.png" ></div>';
    if ($metadata) {
        $to_append .= '<div>';
        $to_append .= '<ul>';
        foreach ($metadata as $key => $meta) {
            $title = str_replace("-"," ", $key);
            $title = str_replace("cfdb7_","", $title);
            $to_append .= '<li><strong>' . $title . ':  &nbsp;</strong>';
            foreach ($meta as $kez => $value) {
                if (its_file($value)) {
                    $value = '<a href="' . $cfdb7_dirname . $value  . '" target="_blank">' . $value . '</a>';
                }
                if (strpos($value, '{i:0;s:5:') !== false) {
                    $value = str_replace('a:1:{i:0;s:5:"',"", $value);
                    $value = str_replace('";}',"", $value);
                }
                if ($key > 0) {
                    $to_append .= ', ' .  $value;
                } else {
                    $to_append .= $value;
                }
            }
            $to_append .= '</li>';
        }
        $to_append .= '</ul>';
        $to_append .= '</div>';
    }
    $styles = "<style>.head{
        background-image: url('/wp-content/themes/imp-internet/img/header-background.jpg');background-repeat: no-repeat;}</style>";
    require_once(get_template_directory() . "/lib/vendor/autoload.php");
    $mpdf = new \Mpdf\Mpdf();

    $mpdf->SetHTMLFooter('
  <table width="100%">
      <tr>
          <td width="33%">{DATE j-m-Y}</td>
          <td width="33%" align="center">{PAGENO}/{nbpg}</td>
          <td width="33%" style="text-align: right;">' . __('Imp internet', 'imp') . '</td>
      </tr>
  </table>');

    $mpdf->WriteHTML($styles . $to_append);
    $mpdf->Output('ticket-' . $ID . '.pdf', 'D');
}
