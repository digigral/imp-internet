<?php

/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();

$container = get_theme_mod('understrap_container_type');
$terms = get_terms( array(
    'taxonomy' => 'kategorija-risb',
    'hide_empty' => true,
) );
?>

<div class="wrapper archive-risbe" id="archive-wrapper">

	<div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">


		<div class="row">
			<div class="col-md-10">
				<header class="page-header">
					<h1 class="page-title">Exploded view</h1>
				</header><!-- .page-header -->
				<?php if (have_posts()) : ?>
					<div class="archive-risbe__list">
						<?php /* Start the Loop */ ?>
						<?php while (have_posts()) : the_post(); ?>

							<a class="archive-risbe__item" href="<?= get_the_permalink(); ?>">
								<div><?= get_the_title(); ?></div>
							</a>

						<?php endwhile; ?>
					</div>

				<?php else : ?>

					<?php get_template_part('loop-templates/content', 'none'); ?>

				<?php endif; ?>

				<!-- The pagination component -->
				<?php understrap_pagination(); ?>

			</div>
			<div class="col-md-2">
				<a class="back__btn" href="/">
					< Back </a>
					
					<?php 
				if($terms):?>
				<div class="filter">
					<div class="filter__title"> Filter:</div>
					<div class="filter__list">
					<?php foreach($terms as $term): ?>
						<a href="<?= get_post_type_archive_link('most-common-errors') . '?kategorija=' . $term->slug; ?>" class="filter__item">
							<?= $term->name; ?>
						</a>
						<?php endforeach; ?>
					</div>
				</div>			
				<?php endif; ?>
			</div>

		</div>
	</div><!-- #content -->

</div><!-- #archive-wrapper -->

<?php get_footer(); ?>