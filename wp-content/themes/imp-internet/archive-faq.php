<?php

/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();

$container = get_theme_mod('understrap_container_type');
?>

<div class="wrapper archive-error-codes" id="archive-wrapper">

	<div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

		<div class="row">
			<div class="col-md-10">
				<header class="page-header">
					<h1 class="page-title">FAQ</h1>
				</header><!-- .page-header -->
				<?php if (have_posts()) : ?>
					<div class="accordion" id="accordionExample">
						<?php /* Start the Loop */ ?>
						<?php while (have_posts()) : the_post(); ?>
						<div class="card">		
					
							<button class="btn btn-light collapsed" type="button" data-toggle="collapse" data-target="#collapse<?= get_the_ID(); ?>" aria-expanded="true" aria-controls="collapse<?= get_the_ID(); ?>">
							<?= get_the_title(); ?>
							<div class="card-tools">
								<i class="fa fa-plus" aria-hidden="true"></i>
								<i class="fa fa-minus" aria-hidden="true"></i>	
							</div>					
							</button>						
							<div id="collapse<?= get_the_ID(); ?>" class="collapse" aria-labelledby="heading<?= get_the_ID(); ?>" data-parent="#accordionExample">
								<div class="card-body">
									<?php the_content(); ?>
								</div>
							</div>
						</div>

						<?php endwhile; ?>
					</div>

				<?php else : ?>

					<?php get_template_part('loop-templates/content', 'none'); ?>

				<?php endif; ?>

				<!-- The pagination component -->
				<?php understrap_pagination(); ?>

			</div>

			<div class="col-md-2">
				<a class="back__btn" href="/">
					< Back </a>
			</div>

		</div>
	</div><!-- #content -->

</div><!-- #archive-wrapper -->

<?php get_footer(); ?>