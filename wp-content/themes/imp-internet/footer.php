<?php

/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

$container = get_theme_mod('understrap_container_type');
?>

<?php get_template_part('sidebar-templates/sidebar', 'footerfull'); ?>

<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-6 col-6">
				<img class="img-fluid" id="footer-logo" src="https://imp-pumps.com/wp-content/themes/imp/img/logo-footer.png" alt="Imp Footer Logo">
			</div>
			<div class="col-lg-3 col-md-6 col-6 d-none d-lg-block">
				<div class="about-us-footer">
					<h3>About Us</h3>

					<p>IMP Pumps d.o.o. is Slovenian manufacturer of pumps and pumping systems located in Komenda in Slovenia.

					</p>
					<a href="https://imp-pumps.com/about-us/" target="_blank" class="btn btn-fourth">Read more</a>

					<br>
					<a target="_blank" style="display:inline-block; margin-top: 20px; border:none; border:none;" href="https://www.linkedin.com/company/imp-pumps/"><img src="https://imp-pumps.com/wp-content/themes/imp/img/002-linkedin.svg " style="width: 30px; height:auto"></a>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-6 d-none d-lg-block">
			</div>
			<div class="col-lg-3 col-md-6 col-6">
				<h3>CONTACT</h3>
				<ul id="footer-contact">
					<li><i class="fa  fa-map-marker" aria-hidden="true"></i>Pod hrasti 28, 1218 Komenda</li>
					<li><i class="fa  fa-phone" aria-hidden="true"></i>+386 (0)1 28 06 400</li>
					<li><i class="fa  fa-print" aria-hidden="true"></i>+386 (0)1 28 06 460</li>
					<li><i class="fa  fa-envelope" aria-hidden="true"></i><img src="https://imp-pumps.com/wp-content/uploads/2019/10/download.png"></li>
				</ul>
			</div>
		</div>
	</div>
</footer>


</div><!-- #page we need this extra closing tag here -->
<?php get_template_part('page-templates/page-parts/symbolys');
wp_footer(); ?>

</body>

</html>