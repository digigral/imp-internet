(function ($, w, d) {
  'use strict'
  $(document).ready(function () {
    $('#noContextMenu').bind('contextmenu', function () { return false; });
  });
  $('.preaptive-check__answer').on('click', 'button', function () {
    const id = '#' + $(this).data('id');
    const text = $(this).text();
    const step = $(this).parent().parent().data('step') + 1;

    $(this).parent().parent().addClass('done');
    $(this).parent().parent().find('.preaptive-check__a').text(text);
    $(id).addClass('active');
    $(id).attr("data-step", step);
  });

  $('.preaptive-check__answer-last').on('click', 'button', function () {
    $('.preaptive-check__contact').addClass('active');
  });

  $('.preaptive-check__edit').on('click', function () {
    const id = '#' + $(this).data('id');
    const estep = $(id).data('step');
    $(id).removeClass('done');
    $(id).find('.preaptive-check__a').text('');
    $('.preaptive-check__item').each(function () {
      const lstep = $(this).data('step');
      if (lstep > estep) {
        $(this).find('.preaptive-check__a').text('');
        $(this).removeClass('done');
        $(this).removeClass('active');
      }
    });
  });

  // Open the Modal
  function openModal() {
    document.getElementById("myModal").style.display = "block";
  }

  // Close the Modal
  function closeModal() {
    document.getElementById("myModal").style.display = "none";
  }

  var slideIndex = 1;
  showSlides(slideIndex);

  // Next/previous controls
  function plusSlides(n) {
    showSlides(slideIndex += n);
  }

  // Thumbnail image controls

  function currentSlide(n) {
    if (n === 1) {
      showSlides(slideIndex = n);
    } else {
      showSlides(slideIndex = $('.mySlides[data-id="' + n + '"]').data('slide'));
    }

  }

  function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");;
    var captionText = document.getElementById("caption");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }
    slides[slideIndex - 1].style.display = "block";
    if ($(slides[slideIndex - 1]).data('cap') && $(slides[slideIndex - 1]).data('cap') !== 'undefined') {
      captionText.innerHTML = $(slides[slideIndex - 1]).data('cap');
      $('.modal__img .caption-container').css('display', 'block');
    } else {
      captionText.innerHTM = '';
      $('.modal__img .caption-container').css('display', 'none')
    }

  }

  $('.galery__item').on('click', function () {
    if ($(this).hasClass('nolightbox')) {
      return;
    } else {
      openModal();
      if ($(this).find('img').attr('class')) {
        currentSlide($(this).find('img').attr('class'));
      } else {
        currentSlide(1);
      }
    }
  });

  $('.main-photo').on('click', function () {
    if ($(this).hasClass('nolightbox')) {
      return;
    } else {
      openModal();
      if ($(this).find('img').attr('class')) {
        currentSlide($(this).find('img').attr('class'));
      } else {
        currentSlide(1);
      }
    }
  });

  $('.modal__img .prev').on('click', function () {
    plusSlides(-1);
  });

  $('.modal__img .next').on('click', function () {
    plusSlides(1);
  });
  $('.modal__img .close').on('click', function () {
    closeModal();
  });

  $('.modal__img__backdrop').on('click', function () {
    closeModal();
  });


  $(function () {
    let images = [];
    $('.main-photo').each(function(){
      images.push({link: $(this).find('img').attr('src'),cap: $(this).find('figcaption').text(), id:$(this).find('img').attr('class') })     
    });
    $('.galery__item').each(function(){
      images.push({link: $(this).find('img').attr('src'),cap: $(this).find('figcaption').text(), id:$(this).find('img').attr('class') })     
    });
    if(images){
      $('.slides').empty();
      images.forEach(function(img, i){
        const item =  '<div class="mySlides" data-cap="'+ img.cap +'" data-id="'+ img.id +'" data-slide="'+ (i+1) +'">'
       +'<div class="numbertext">'+ (i+1) +' / '+ images.length +'</div>'
       +'<img src="'+ img.link +'"+ style="width:100%"></div>';
        $('.slides').append(item);
      });
    }
  });
})(jQuery, window, document);
