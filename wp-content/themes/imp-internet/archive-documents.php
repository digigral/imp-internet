<?php

/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();

$container = get_theme_mod('understrap_container_type');
?>

<div class="wrapper archive-risbe" id="archive-wrapper">

	<div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">
		<div class="row">
			<div class="col-md-10">
				<header class="page-header">
					<h1 class="page-title">Documents</h1>
				</header><!-- .page-header -->
				<?php if (have_posts()) : ?>
					<div class="archive-risbe__list">
						<?php /* Start the Loop */ ?>
						<?php while (have_posts()) : the_post(); ?>
							<a class="archive-risbe__item" href="<?= get_field('file',get_the_ID()); ?>" target="_blank">
								<div><?= get_the_title(); ?></div>
							</a>
						<?php endwhile; ?>
					</div>
				<?php else : ?>
					<?php get_template_part('loop-templates/content', 'none'); ?>
				<?php endif; ?>
				<!-- The pagination component -->
				<?php understrap_pagination(); ?>
			</div>
			<div class="col-md-2">
				<a class="back__btn" href="/">
					< Back </a>
			</div>
		</div>
	</div><!-- #content -->

</div><!-- #archive-wrapper -->

<?php get_footer(); ?>