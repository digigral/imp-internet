<?php

/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();

$container = get_theme_mod('understrap_container_type');
?>

<div class="wrapper archive-error-codes" id="archive-wrapper">

	<div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

		<div class="row">
			<div class="col-md-10">
				<header class="page-header">
					<h1 class="page-title">Error codes</h1>
				</header><!-- .page-header -->
				<?php if (have_posts()) : ?>
					<div class="accordion" id="accordionMain">
						<?php /* Start the Loop */ ?>
						<?php while (have_posts()) : the_post(); ?>
							<div class="card">
								<button class="btn btn-light collapsed" type="button" data-toggle="collapse" data-target="#collapse<?= get_the_ID(); ?>" aria-expanded="true" aria-controls="collapse<?= get_the_ID(); ?>">
									<?= get_the_title(); ?>
									<div class="card-tools">
										<i class="fa fa-plus" aria-hidden="true"></i>
										<i class="fa fa-minus" aria-hidden="true"></i>
									</div>
								</button>
								<div id="collapse<?= get_the_ID(); ?>" class="collapse" aria-labelledby="heading<?= get_the_ID(); ?>" data-parent="#accordionMain">
									<div class="card-body">
										<div class="accordion" id="accordionSubitems">
										<?php
										$list_of_possible_erros = get_field('list_of_possible_erros');
										
										if ($list_of_possible_erros) :
											foreach ($list_of_possible_erros as $key=>$item) : ?>
											<div class="card det-item">
												<button class="btn btn-light <?= count($list_of_possible_erros) <2 ? '' : 'collapsed'; ?>" type="button" data-toggle="collapse" data-target="#collapse-<?= get_the_ID(); ?><?= $key ?>" aria-expanded="true" aria-controls="collapse-<?= get_the_ID(); ?><?= $key ?>">
													Electronical compartment: <?=  $item['electronical_compartment']; ?>
													<div class="card-tools">
														<i class="fa fa-plus" aria-hidden="true"></i>
														<i class="fa fa-minus" aria-hidden="true"></i>
													</div>
												</button>
												<div id="collapse-<?= get_the_ID(); ?><?= $key ?>" class="collapse  <?= count($list_of_possible_erros) <2 ? 'show' : ''; ?>  " aria-labelledby="heading-<?= get_the_ID(); ?><?= $key ?>" data-parent="#accordionSubitems">
													<div class="card-body">
														<h3 class="det-title">Pumps</h3>
														<div class="det-content">
															<?= $item['pumps']; ?>
														</div>
														<h3 class="det-title">Fault description</h3>
														<div class="det-content">
															<?= $item['fault_description']; ?>
														</div>
														<h3 class="det-title">Cause</h3>
														<div class="det-content">
															<?= $item['cause']; ?>
														</div>

														<h3 class="det-title">Solution</h3>
														<div class="det-content">
															<?= $item['solution']; ?>
														</div>
													</div>
												</div>
											</div>
										<?php endforeach;
										endif; ?>
										</div>
									</div>
								</div>
							</div>

						<?php endwhile; ?>
					</div>

				<?php else : ?>

					<?php get_template_part('loop-templates/content', 'none'); ?>

				<?php endif; ?>

				<!-- The pagination component -->
				<?php understrap_pagination(); ?>

			</div>

			<div class="col-md-2">
				<a class="back__btn" href="/">
					< Back </a>
			</div>

		</div>
	</div><!-- #content -->

</div><!-- #archive-wrapper -->

<?php get_footer(); ?>