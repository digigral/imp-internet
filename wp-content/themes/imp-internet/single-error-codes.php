<?php

/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();
$container = get_theme_mod('understrap_container_type');
$errors = get_field('napaka_na_pumpi');
$pumps = [];
if ($errors) {
	foreach ($errors as $error) {
		$pumps[] = $error['pumpa'];
	}
}
?>

<div class="wrapper single-most-common-errors" id="single-wrapper">

	<div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">
		<div class="row">
			<div class="col-md-10">
				<h1 class="page-title"><?= get_the_title(); ?></h1>
				<?php
				$list_of_possible_erros = get_field('list_of_possible_erros');

				if ($list_of_possible_erros) :
					foreach ($list_of_possible_erros as $key => $item) : ?>	
						<div class="item">		
							<h2>Electronical compartment: <?= $item['electronical_compartment']; ?></h2>
							<div>
								<h3 class="det-title">Pumps</h3>
								<div class="det-content">
									<?= $item['pumps']; ?>
								</div>
								<h3 class="det-title">Fault description</h3>
								<div class="det-content">
									<?= $item['fault_description']; ?>
								</div>
								<h3 class="det-title">Cause</h3>
								<div class="det-content">
									<?= $item['cause']; ?>
								</div>
								<h3 class="det-title">Solution</h3>
								<div class="det-content">
									<?= $item['solution']; ?>
								</div>
							</div>
						</div>
				<?php endforeach;
				endif; ?>
			</div>
			<div class="col-md-2">
				<a class="back__btn" href="<?= get_post_type_archive_link('error-codes'); ?>">
					< Back </a>
			</div>
		</div>
	</div><!-- #content -->

</div><!-- #single-wrapper -->

<?php get_footer(); ?>