<?php
/**
 * The template for displaying search results pages.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="search-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<main class="site-main" id="main">


				<?php if ( have_posts() ) : ?>
					<header class="page-header">
						<h1 class="page-title">
							<?= __( 'Search Results:', 'understrap' ); ?>
						</h1>
					</header><!-- .page-header -->
					<div class="search_s">
						<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
					</div>					
					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>
					<div class="search-item">
						<h2 class="entry-title"><a href="http://imp-internet.digiapps.my/error-codes/e25/" rel="bookmark">E25</a></h2>
						<div>
							<?php get_the_excerpt($post); ?>
						</div>
					</div>
					<?php endwhile; ?>
				<?php else : ?>

					<?php get_template_part( 'loop-templates/content', 'none' ); ?>

				<?php endif; ?>

			</main><!-- #main -->

			<!-- The pagination component -->
			<?php understrap_pagination(); ?>



		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #search-wrapper -->

<?php get_footer(); ?>
