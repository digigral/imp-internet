<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero' ); ?>
<?php endif; ?>

<div class="" id="index-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
	<?php if(!is_user_logged_in()): ?>
			<div class="">
				<?= do_shortcode('[ultimatemember form_id="90"]'); ?>
			</div>
			<?php else: ?>
		<div class="service__list">		
			<div class="service__item">
				<a href="<?= get_post_type_archive_link('exploded-view'); ?>">
					<img src="/wp-content/themes/imp-internet/img/explode.png">
					<span class="service__item-text">Exploded view</span>
				</a>
			</div>
			<div class="service__item">
				<a href="<?= get_post_type_archive_link('most-common-errors'); ?>">
				<img src="/wp-content/themes/imp-internet/img/most-com-e.png">				
					<span class="service__item-text">Most common errors</span>
				</a>
			</div>
			<div class="service__item">
				<a href="<?= get_post_type_archive_link('documents'); ?>">
				<img src="/wp-content/themes/imp-internet/img/doc.png">
					<span class="service__item-text">Documents</span>
				</a>
			</div>
			<div class="service__item">
				<a href="<?= get_post_type_archive_link('error-codes'); ?>">
					<img src="/wp-content/themes/imp-internet/img/err.png">		
					<span class="service__item-text">Error codes</span>
				</a>
			</div>
			<div class="service__item">
				<a href="<?= get_post_type_archive_link('videos'); ?>">
					<img src="/wp-content/themes/imp-internet/img/video.png">
					<span class="service__item-text">Videos</span>
				</a>
			</div>
			<div class="service__item">
				<a href="/pre-emtive-check">
					<img src="/wp-content/themes/imp-internet/img/pre-e-c.png">
					<span class="service__item-text">Pre emtive check</span>
				</a>
			</div>	
			<div class="service__item">
				<a href="/faq">
					<img src="/wp-content/themes/imp-internet/img/faq.png">
					<span class="service__item-text">FAQ</span>
				</a>
			</div>
			<div class="service__item">
				<a href="/news">
					<img src="/wp-content/themes/imp-internet/img/news.png">
					<span class="service__item-text">News</span>
				</a>
			</div>
		</div>
		<?php endif; ?>

	</div><!-- #content -->
	<div class="frontpage__search">
		<div class="container">
			<div class="frontpage__search-warp">
				<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
			</div>			
		</div>			
	</div>

</div><!-- #index-wrapper -->

<?php get_footer(); ?>
